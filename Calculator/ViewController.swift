//
//  ViewController.swift
//  Calculator
//
//  Created by Soeng Saravit on 10/25/17.
//  Copyright © 2017 Soeng Saravit. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // Stored Properit
    var answer:Double?
    var operate:String?
    var num1:Double?
    var num2:Double?
    var bool:Bool = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    //Connection Outlet of Label
    @IBOutlet weak var showLabelA: UILabel!
    @IBOutlet weak var showLabelB: UILabel!
    
    //Action of button
    @IBAction func number(_ sender: UIButton) {
        let number = sender.currentTitle
        //var title = showLabelA.text
        if showLabelA.text == "0" {
            showLabelA.text = ""
        }else if showLabelA.text == "-0" {
            showLabelA.text = "-"
        }
        
        // set value when button pressed
        if number == "0" {
            if showLabelA.text == "0" {
                showLabelA.text = number
            }
            else {
                showLabelA.text = showLabelA.text! + number!
            }
        } else {
            showLabelA.text = showLabelA.text! + number!
        }
    }
    @IBAction func actionButton(_ sender: UIButton) {
        operate = sender.currentTitle!
        if bool == true {
            showLabelB.text = ""
            bool = false
        }
        if showLabelB.text == "" {
            num1 = Double(showLabelA.text!)
            num2 = 0.0
            showLabelB.text = showLabelA.text! + " \(operate!) "
            showLabelA.text = "0"
        }else {
            switch operate {
            case "+/-":
                num2 = Double(showLabelA.text!)
                answer = num1! - num2!
                showLabelB.text = removeDecimal(decimal: answer!) + " \(operate!) "
                showLabelA.text = "0"
                num1 = answer
                num2 = 0.0
            case "%":
                num2 = Double(showLabelA.text!)
                answer = num1!.truncatingRemainder(dividingBy: num2!)
                showLabelB.text = removeDecimal(decimal: answer!) + " \(operate!) "
                showLabelA.text = "0"
                num1 = answer
                num2 = 0.0
            case "÷":
                num2 = Double(showLabelA.text!)
                answer = num1! / num2!
                showLabelB.text = removeDecimal(decimal: answer!) + " \(operate!) "
                showLabelA.text = "0"
                num1 = answer
                num2 = 0.0
            case "×":
                num2 = Double(showLabelA.text!)
                answer = num1! * num2!
                showLabelB.text = removeDecimal(decimal: answer!) + " \(operate!) "
                showLabelA.text = "0"
                num1 = answer
                num2 = 0.0
            case "-":
                num2 = Double(showLabelA.text!)
                answer = num1! - num2!
                showLabelB.text = removeDecimal(decimal: answer!) + " \(operate!) "
                showLabelA.text = "0"
                num1 = answer
                num2 = 0.0
            case "+":
                num2 = Double(showLabelA.text!)
                showLabelB.text = removeDecimal(decimal: answer!) + " \(operate!) "
                answer = num1! + num2!
                showLabelA.text = "0"
                num1 = answer
                num2 = 0.0
            default:
            print("default work")
            }
        }
        
    }
    @IBAction func resultButton(_ sender: UIButton) {
        if num1 != nil && num2 != nil {
            num2 = Double(showLabelA.text!)
            if num2! < 0 {
                showLabelB.text! += " (" + showLabelA.text! + ")"
            }else {
                showLabelB.text! = showLabelB.text! + showLabelA.text!
            switch operate {
            case "+":
                answer = num1! + num2!
            case "-":
                answer = num1! - num2!
            case "×":
                answer = num1! * num2!
            case "÷":
                answer = num1! / num2!
            case "%":
                answer = num1!.truncatingRemainder(dividingBy: num2!)
            default:
                print("Oparator invalid")
            }
                showLabelA.text = removeDecimal(decimal: answer!)
                bool = true
                num1 = 0
                num2 = 0
            }
        }else {
            return
        }
    }
    @IBAction func clearButton(_ sender: UIButton) {
        showLabelA.text! = "0"
        showLabelB.text = ""
    }
}

func setButtonColor(button: UIButton){
    button.backgroundColor = UIColor.white
    button.setTitleColor(UIColor.black, for: .normal)
}

func removeDecimal(decimal:Double) -> String {
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
    formatter.decimalSeparator = "."
    formatter.groupingSeparator = ""
    let str = formatter.string(from: decimal as NSNumber) ?? "Error"
    if str == "NaN" || str == "+∞" || str == "-∞"{
        return "Error"
    }
    return str
    
}
@IBDesignable class customButton: UIButton {
    
    @IBInspectable
    public var cornerRadius: CGFloat = 0.0 {
        didSet {
           self.layer.cornerRadius = self.cornerRadius
        }
    }
}
